﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Body : Detail
    {
        public Body() : base()
        {
            DetailName = "Body";
            Elements.Add(new SquareElement("Iron", 5.6));
            Elements.Add(new SquareElement("Iron", 5.6));
            Elements.Add(new SquareElement("Iron", 5.6));
        }

        public Body(Body body)
        {
            Elements = new List<Element>(body.Elements);
            DetailName = new String(body.DetailName);
        }

        public Body Copy()
        {
            return new Body(this);
        }

        public override object Clone()
        {
            return new Body(this);
        }

    }
}
