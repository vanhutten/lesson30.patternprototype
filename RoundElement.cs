﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class RoundElement : Element 
    {
        public RoundElement( string _material, double _mass) : base("Round Element", _material, _mass)
        {
            
        }

    }
}
