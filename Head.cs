﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Head : Detail
    {
        
        public Head () : base()
        { 
            DetailName = "Head";
            Elements.Add(new RoundElement("Iron", 1.2));
        }
        
        public Head (Head head)
        {
            DetailName = head.DetailName;
            Elements = new List<Element>(head.Elements);
        }

        public Head Copy()
        {
            return new Head(this);
        }

        public override object Clone()
        {
            return new Head(this);
        }
    }
}
