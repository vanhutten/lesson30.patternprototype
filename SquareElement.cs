﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class SquareElement : Element
    {
        public SquareElement(string _material, double _mass) : base("Square Element", _material, _mass)
        {
            
        }

    }
}
