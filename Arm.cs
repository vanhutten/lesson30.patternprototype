﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Arm : Detail, IMyClonable<Arm>, ICloneable
    {

        public Arm(Arm arm)
        {
            DetailName = new string(arm.DetailName);
            Elements = new List<Element>(arm.Elements);
        }

        public Arm()
        {
            Elements = new List<Element>();
            Elements.Add(new SquareElement("Iron", 0.7));
            Elements.Add(new RoundElement("Iron", 0.4));
            Elements.Add(new SquareElement("Iron", 0.7));
            Elements.Add(new SquareElement("Iron", 0.2));
        }

        public Arm Copy()
        {
            return new Arm(this);
        }

        public object Clone()
        {
            return new Arm(this);
        }
    }
}
