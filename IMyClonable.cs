﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    interface IMyClonable<out T>
    {
      T Copy();
    }
}
