﻿using System;

namespace Lesson30.PatternPrototype
{
    /// <summary>
    /// Домашнее задание паттерн Прототип
    /// </summary>
    static class Program
    {
        static void Main(string[] args)
        {
          
            //Create Robot1
            Robot Robot1 = new Robot(new Arm(), new Arm(), new Leg(), new Leg(), new Body(), new Head());
            Robot Robot2 = Robot1.Copy();
            Console.WriteLine($"Robot1 {Robot1} ");
            Console.WriteLine($"Robot2 {Robot2} ");

            Robot Robot3 = (Robot)Robot1.Clone();
            Console.WriteLine($"Robot3 {Robot3} ");
            
            // Преимуществом интерфейса MyIClonable является, отсутствие необходимости приводить типы. Для IClonable есть необходимость
            // приводить каждый объект к нужному типу.

            
            // Преимуществом интерфейса MyIClonable является, отсутствие необходимости приводить типы. Для IClonable есть необходимость
            // приводить каждый объект к нужному типу.

        }
    }
}
