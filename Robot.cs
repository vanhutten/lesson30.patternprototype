﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Robot : IMyClonable <Robot>, ICloneable
    {

        internal Arm LeftArm { get ; set ; }
        internal Arm RightArm { get ; set ; }
        internal Leg LeftLeg { get ; set ; }
        internal Leg RightLeg { get ; set ; }
        internal Body Torso { get ; set ; }
        internal Head BigHead { get ; set ; }

        public Robot(Arm leftArm, Arm rightArm, Leg leftLeg, Leg rightLeg, Body torso, Head bigHead)
        {
            LeftArm = leftArm;
            RightArm = rightArm;
            LeftLeg = leftLeg;
            RightLeg = rightLeg;
            Torso = torso;
            BigHead = bigHead;
        }

        public Robot Copy()
        {
            return new Robot(leftArm: LeftArm.Copy(), 
                rightArm: RightArm.Copy(), 
                leftLeg: LeftLeg.Copy(), 
                rightLeg: RightLeg.Copy(), 
                torso: Torso.Copy(), 
                bigHead: BigHead.Copy());
        }

        public override string ToString()
        {
            return new string(LeftArm.ToString() + RightArm.ToString() + BigHead.ToString() + Torso.ToString() +
                LeftLeg.ToString() + RightLeg.ToString());
        }

        public object Clone()
        {
            return new Robot(leftArm: (Arm)LeftArm.Clone(),
                rightArm: (Arm)RightArm.Clone(),
                leftLeg: (Leg)LeftLeg.Clone(),
                rightLeg: (Leg)RightLeg.Clone(),
                torso: (Body)Torso.Clone(),
                bigHead: (Head)BigHead.Clone());
        }
    }
}
