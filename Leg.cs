﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Leg : Detail
    {
        
        public Leg(Leg leg)
        {
            DetailName = new string(leg.DetailName);
            Elements = new List<Element>(leg.Elements);
        }

        public Leg()
        {
            Elements = new List<Element>
            {
                new SquareElement("Iron", 1.5),
                new RoundElement("Iron", 1.1),
                new SquareElement("Iron", 1.5)
            };
        }

        public Leg Copy()
        {
            return new Leg(this);
        }

        public override object Clone()
        {
            return new Leg(this);
        }
    }
}
