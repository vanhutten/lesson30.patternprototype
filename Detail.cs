﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    class Detail : IMyClonable<Detail>, ICloneable
    {
        public List<Element> Elements;
        public string DetailName;
        public void Build(int NumberOfElements, Element element)
        {
            Console.WriteLine("Building Elements");
            Elements = new List<Element>();
            for (int i=0; i< NumberOfElements; i++)
            {
                Elements.Add(element.Copy());
            }
        }

        public Detail() 
        { 
            Elements = new List<Element>(); 
            DetailName = "Detail";
        }


        public virtual Detail Copy()
        {
            var detail = new Detail();
            if (Elements.Count > 0)
                detail.Build(Elements.Count, Elements[0]);
            return detail;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder($"/Detail: {DetailName}");
            
            foreach(var item in Elements)
            {
                str.Append(item.ToString());
            }
            str.Append("/");
            return str.ToString();
        }

        public virtual object Clone()
        {
            var detail = new Detail()
            {
                DetailName = (string)DetailName.Clone(),
                Elements = new List<Element>(Elements)
            };
            return detail;
        }
    }
}
