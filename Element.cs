﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson30.PatternPrototype
{
    public class Element : IMyClonable<Element>, ICloneable
    {
        readonly string Material;
        readonly string Name;
        readonly double Mass;

        public Element(string _name, string _material, double _mass)
        {
            Name = _name;
            Material = _material;
            Mass = _mass;
        }

        public object Clone()
        {
           return new Element(Name, Material, Mass);
        }

        public virtual Element Copy()
        {
            return new Element(Name, Material, Mass);
        }

        public override string ToString()
        {
            return new string($"|ElemName: {Name} , Material: {Material}, Weight: {Mass}|");
        }
    }
}
